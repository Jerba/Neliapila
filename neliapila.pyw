import platform
from tkinter import *
import urllib.request
from urllib.error import URLError, HTTPError
import pathlib
import _thread

cur_platform = platform.system().lower()

window = Tk()
if "linux" in cur_platform:
    window.geometry("470x300")
else:
    window.geometry("420x300")
window.title("Neliapila")
window.resizable(False, False)
window.configure(bg="gray94")

if "linux" not in cur_platform and pathlib.Path("neliapila_32.ico").is_file():
    window.iconbitmap("neliapila_32.ico")

webm_text       = ""
webm_text_old   = ""
gif_text        = "" 
gif_text_old    = "" 
image_text      = ""
image_text_old  = ""

stop_download       = 0
ripping_in_progress = 0
webm_in_progress    = 0
gif_in_progress     = 0
image_in_progress   = 0

if "linux" in cur_platform:
    cb_w        = 18
    cb_y1       = 96
    cb_y2       = 126
    cb_img_x    = 40
    cb_gif_x    = 29
    cb_web_x    = 194
    cb_sub_x    = 193
    entry_url   = Entry(window, bd = 3, width = 42)
    entry_url.place(x = 60, y = 60)
else:
    cb_w        = 20
    cb_y1       = 90
    cb_y2       = 120
    cb_img_x    = 33
    cb_gif_x    = 24
    cb_web_x    = 184
    cb_sub_x    = 182
    entry_url   = Entry(window, bd = 3, width = 49)
    entry_url.place(x = 60, y = 60)

def RequestData(url, headers):
    if headers == 0:
        request = urllib.request.Request(url)
    else:
        request = urllib.request.Request(url, None, headers)

    try:
        response = urllib.request.urlopen(request, None)
    except HTTPError as e:
        return 0
    except URLError as e:
        return 0
    data = response.read()
    return data

def CreateLabel(label_text, label_x, label_y, label_font):
    lb = Label(window, text = label_text)
    if label_font == 0:
        lb.config(bg = "gray94")
    else:
        lb.config(font = label_font, bg = "gray94")
    lb.place(x = label_x, y = label_y)
    return lb

def CreateCheckbutton(cb_text, cb_var, cb_x, cb_y):
    cb = Checkbutton(window, text = cb_text, variable = cb_var, \
                    onvalue = 1, offvalue = 0, height = 1, width = cb_w)
    cb.configure(bg = "gray94", activebackground = "gray94", \
                highlightbackground = "gray94", highlightcolor = "gray94", \
                highlightthickness = 0)
    cb.place(x = cb_x, y = cb_y)
    cb_var.set(1)
    return cb

def ChangeUpdater():
    global webm_text, webm_text_old, gif_text, gif_text_old, \
            image_text, image_text_old
    global stop_download, ripping_in_progress, image_in_progress, \
            gif_in_progress, webm_in_progress

    if webm_text != webm_text_old:
        window.after(0, ChangeLabelText, label_webms, webm_text)
    if gif_text != gif_text_old:
        window.after(0, ChangeLabelText, label_gifs, gif_text)
    if image_text != image_text_old:
        window.after(0, ChangeLabelText, label_images, image_text)

    if ripping_in_progress == 1 and image_in_progress == 0 and \
        gif_in_progress == 0 and webm_in_progress == 0:
        stop_download       = 0
        ripping_in_progress = 0
        window.after(0, UpdateErrorLabel, "Done!", "gray94")
        window.after(0, ChangeButtonText, button_submit, "RIP!")

        if "Fetching" in label_webms.cget("text"):
            window.after(4, ChangeLabelText, label_webms, "Webm: Done!")
        if "Fetching" in label_gifs.cget("text"):
            window.after(4, ChangeLabelText, label_gifs, "Gif: Done!")
        if "Fetching" in label_images.cget("text"):
            window.after(4, ChangeLabelText, label_images, "Image: Done!")

    webm_text_old   = webm_text
    gif_text_old    = gif_text
    image_text_old  = image_text
    window.after(1, ChangeUpdater)

def ChangeButtonText(button_to_update, button_text):
    button_to_update.config(text = button_text)

def ChangeLabelText(label_to_update, label_text):
    label_to_update.config(text = label_text)

def UpdateErrorLabel(error_text, error_colour):
    label_error.config(text = error_text, bg = error_colour)

def CreateSubfolder(thread_name, subfolder):
    if create_subs.get() == 1:
        pathlib.Path("downloads/" + thread_name + "/" + \
        subfolder).mkdir(parents = True, exist_ok = True)

def GetFilepath(thread_name, subfolder, filename):
    if create_subs.get() == 1:
        return "downloads/" + thread_name  + "/" + subfolder + "/" + filename
    else:
        return "downloads/" + thread_name + "/" + filename

def AddItemToList(thread_name, val, items, subfolder, download_data):
    item = val.split("//", 1)[-1]
    item = item.split('"', 1)[0]

    filename = item.split("/", 3)[2]
    filepath = GetFilepath(thread_name, subfolder, filename)

    file = pathlib.Path(filepath)
    if not file.is_file():
        download_data[2] += 1
        items.append(item)
    else:
        download_data[1] += 1
    download_data[0] += 1

def GetData(item, download_data):
    item_data = RequestData("https://" + item, 0)
    if item_data == 0:
        download_data[4] += 1
    return item_data

def WriteData(thread_name, item, item_data, subfolder):
    filename = item.split('/', 3)[2]
    filepath = GetFilepath(thread_name, subfolder, filename)

    file = open(filepath, "wb")
    file.write(item_data)
    file.close

def DownloadImages(thread_name, splitdata):
    global stop_download
    global image_text
    image_text = "Image: Fetching Urls..."

    rip_info = [0, 0, 0, 0 ,0]
    items = []

    for val in splitdata:
        if stop_download == 1:
            break
        if "is2" not in val or "class" in val or ".gif" in val:
            continue
        AddItemToList(thread_name, val, items, "Images", rip_info)

    CreateSubfolder(thread_name, "Images")
    for item in items:
        if stop_download == 1:
            break

        rip_info[3] += 1
        image_text = "Image: Downloading " + str(rip_info[3]) + "/" + \
                    str(rip_info[2])

        item_data = GetData(item, rip_info)
        if item_data == 0:
            continue

        WriteData(thread_name, item, item_data, "Images")

    if stop_download == 1:
        image_text = "Image: Download Stopped!"
    else:
        image_text = "Image: Done! (" + str(rip_info[0]) + " links, already had " + \
                str(rip_info[1]) + ", " + str(rip_info[4]) + " failed)"
    global image_in_progress
    image_in_progress = 0

def DownloadGifs(thread_name, splitdata):
    global stop_download
    global gif_text
    gif_text = "Gif: Fetching Urls..."

    rip_info = [0, 0, 0, 0 ,0]
    items = []

    for val in splitdata:
        if stop_download == 1:
            break
        if "is2" not in val or "class" in val or ".gif" not in val:
            continue
        AddItemToList(thread_name, val, items, "Gifs", rip_info)

    CreateSubfolder(thread_name, "Gifs")
    for item in items:
        if stop_download == 1:
            break

        rip_info[3] += 1
        gif_text = "Gif: Downloading " + str(rip_info[3]) + "/" + str(rip_info[2])

        item_data = GetData(item, rip_info)
        if item_data == 0:
            continue

        WriteData(thread_name, item, item_data, "Gifs")

    if stop_download == 1:
        gif_text = "Gif: Download Stopped!"
    else:
        gif_text = "Gif: Done! (" + str(rip_info[0]) + " links, already had " + \
                str(rip_info[1]) + ", " + str(rip_info[4]) + " failed)"
    global gif_in_progress
    gif_in_progress = 0

def DownloadWebms(thread_name, splitdata):
    global stop_download
    global webm_text
    webm_text = "Webm: Fetching Urls..."

    rip_info = [0, 0, 0, 0 ,0]
    items = []

    for val in splitdata:
        if stop_download == 1:
            break
        if "is3" not in val or "class" in val:
            continue
        AddItemToList(thread_name, val, items, "Webms", rip_info)

    CreateSubfolder(thread_name, "Webms")
    for item in items:
        if stop_download == 1:
            break

        rip_info[3] += 1
        webm_text = "Webm: Downloading " + str(rip_info[3]) + "/" + str(rip_info[2])

        item_data = GetData(item, rip_info)
        if item_data == 0:
            continue

        WriteData(thread_name, item, item_data, "Webms")

    if stop_download == 1:
        webm_text = "Webm: Download Stopped!"
    else:
        webm_text = "Webm: Done! (" + str(rip_info[0]) + " links, already had " + \
                str(rip_info[1]) + ", " + str(rip_info[4]) + " failed)"
    global webm_in_progress
    webm_in_progress = 0

def RipButton():
    user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
    headers={'User-Agent':user_agent,} 

    global ripping_in_progress, image_in_progress, gif_in_progress, webm_in_progress
    global image_text, gif_text, webm_text

    if ripping_in_progress == 1:
        global stop_download
        stop_download = 1
        window.after(0, UpdateErrorLabel, "Stopping download...", "gray94")
        return

    url = entry_url.get()
    if url == "":
        window.after(0, UpdateErrorLabel, "URL is empty", "red")
        return

    if "https://" not in url and "http://" not in url:
        url = "https://" + url

    data = RequestData(url, headers)
    if data == 0:
        window.after(0, UpdateErrorLabel, "Error getting data, check URL", "red")
        return
    splitdata = str(data).split('<')

    thread_name = ""
    for val in splitdata:
        if "thread" not in val:
            continue
        temp = val.split("thread/", 1)[-1]
        temp = temp.split('">', 1)[0]
        temp_arr = temp.split("/")
        thread_name = temp_arr[1] + "_" + temp_arr[0]
        break

    if thread_name == "":
        window.after(0, UpdateErrorLabel, "URL is not valid 4chan link", "red")
        return

    if download_gifs.get() == 0 and download_images.get() == 0 and download_webms.get() == 0:
        window.after(0, UpdateErrorLabel, "No downloads selected!", "red")
        return

    window.after(0, UpdateErrorLabel, "", "gray94")
    pathlib.Path("downloads/" + thread_name).mkdir(parents = True, exist_ok = True)

    ripping_in_progress = 1
    ChangeButtonText(button_submit, "STOP!")

    if download_images.get() == 0:
        image_text = "Image:"
    elif image_in_progress == 0:
        image_in_progress = 1
        _thread.start_new_thread(DownloadImages, (thread_name, splitdata,))

    if download_gifs.get() == 0:
        gif_text = "Gif:"
    elif gif_in_progress == 0:
        gif_in_progress = 1
        _thread.start_new_thread(DownloadGifs, (thread_name, splitdata,))

    if download_webms.get() == 0:
        webm_text = "Webm:"
    elif webm_in_progress == 0:
        webm_in_progress = 1
        _thread.start_new_thread(DownloadWebms, (thread_name, splitdata,))


title_font = ("default", 20, "bold")
label_main      = CreateLabel("4chan Thread Ripper", 70, 10, title_font)
label_url       = CreateLabel("URL:", 20, 60, 0)
label_error     = CreateLabel("", 100, 190, 0)
label_webms     = CreateLabel("Webm:", 80, 220, 0)
label_gifs      = CreateLabel("Gif:", 80, 240, 0)
label_images    = CreateLabel("Image:", 80, 260, 0)

download_images = IntVar()
download_gifs   = IntVar()
download_webms  = IntVar()
create_subs     = IntVar()

checkbutton_images  = CreateCheckbutton("Download Images", download_images, cb_img_x, cb_y1)
checkbutton_gifs    = CreateCheckbutton("Download Gifs", download_gifs, cb_gif_x, cb_y2)
checkbutton_webms   = CreateCheckbutton("Download Webms", download_webms, cb_web_x, cb_y1)
checkbutton_subs    = CreateCheckbutton("Create Subfolders", create_subs, cb_sub_x, cb_y2)

button_submit = Button(window, text = "RIP!", command = RipButton, width = "30")
button_submit.configure(bg = "gray94")
button_submit.place(x = 100, y = 160)

window.after(1, ChangeUpdater)
window.mainloop()
