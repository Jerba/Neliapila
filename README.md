## Neliapila

Neliapila is a simple Python script with GUI to download all images,
gifs and/or webms from a 4chan thread.

### How to use
1. Copy and paste URL of the thread you want to download in the "URL" field.
2. Select which file types you want to download and if you want to create subfolders for them.
3. Press RIP! button and wait until the downloads are complete.
   * Optionally, press the button again (STOP!) to stop downloading.
4. Downloads are found in the same folder as the script/exe, inside "downloads" folder with same name as the thread.

### Standalone Release

##### Windows 7/10:
1. Download [Neliapila.zip](/uploads/e3473bb2489a0413df7787868f5914b4/Neliapila.zip)
2. Extract the downloaded Neliapila.zip
3. Run neliapila.exe

### Running the script

##### Windows 7/10:
1. Install Python 3.6.5
2. Run neliapila.pyw

##### Linux:
1. Install Python 3.6.5 with "sudo apt-get install python 3.6.5"
2. Install Tkinter with "sudo apt-get install python3-tk"
3. Run neliapila.pyw with "python3 neliapila.pyw"


###### Tested on Ubuntu 18.04 and Windows 10